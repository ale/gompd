package mpd

import (
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"sync"
)

// Range represents a range of song indexes. The END item is not
// included in the range.
type Range struct {
	Start, End int
}

// Len returns the total size of the range.
func (r Range) Len() int {
	return r.End - r.Start
}

func (r Range) Contains(i int) bool {
	return (i >= r.Start && i < r.End)
}

// Bound the range to match a given slice (if it extends beyond it, it
// will be limited).
func (r *Range) Bound(slicelen int) {
	if r.Start < 0 {
		r.Start = 0
	}
	if r.Start >= slicelen {
		r.Start = slicelen - 1
	}
	if r.End < 0 {
		r.End = 0
	}
	if r.End > slicelen {
		r.End = slicelen
	}
}

func singleIndexRange(i int) Range {
	return Range{Start: i, End: i + 1}
}

// ParseRange parses a START:END integer range according to the MPD
// protocol spec. Note that in the MPD specification, the END element
// is included in the range.
func ParseRange(s string) (Range, error) {
	var r Range
	if strings.Contains(s, ":") {
		parts := strings.Split(s, ":")
		if len(parts) != 2 {
			return r, errors.New("bad range format")
		}
		var err error
		r.Start, err = strconv.Atoi(parts[0])
		if err != nil {
			return r, err
		}
		end, err := strconv.Atoi(parts[1])
		if err != nil {
			return r, err
		}
		r.End = end + 1
		if r.Start > r.End {
			return r, errors.New("malformed range")
		}
	} else {
		i, err := strconv.Atoi(s)
		if err != nil {
			return r, err
		}
		r.Start = i
		r.End = i + 1
	}
	return r, nil
}

type orderedSongList struct {
	// Store separately a list of songs (identified by their index
	// in the array), and an ordered list of indices, representing
	// the order in which the songs should be visited.
	songs     []Song
	songOrder []int
}

func (l *orderedSongList) Len() int { return len(l.songs) }

func (l *orderedSongList) Empty() bool { return len(l.songs) == 0 }

func (l *orderedSongList) SongAtIndex(idx int) Song {
	if idx < 0 || idx >= len(l.songs) {
		return nil
	}
	return l.songs[idx]
}

func (l *orderedSongList) SongAtPos(pos int) Song {
	if pos < 0 || pos >= len(l.songs) {
		return nil
	}
	idx := l.songOrder[pos]
	return l.songs[idx]
}

func (l *orderedSongList) PositionOf(song Song) int {
	for i, j := range l.songOrder {
		if l.songs[j] == song {
			return i
		}
	}
	return -1
}

func (l *orderedSongList) IndexAtPos(idx int) int {
	if idx < 0 || idx >= len(l.songOrder) {
		return -1
	}
	return l.songOrder[idx]
}

// Add a song. The specified index is actually ignored. Returns the
// new index of the added song.
func (l *orderedSongList) Add(song Song, idx int) int {
	idx = len(l.songs)
	l.songs = append(l.songs, song)
	l.songOrder = append(l.songOrder, idx)
	return idx
}

func (l *orderedSongList) Delete(r Range) {
	r.Bound(len(l.songs))

	// Remove from songs.
	out := make([]Song, len(l.songs)-r.Len())
	copy(out[:r.Start], l.songs[:r.Start])
	copy(out[r.Start:len(out)], l.songs[r.End:len(l.songs)])
	l.songs = out

	// Remove from song index.
	var tmp []int
	for _, j := range l.songOrder {
		if j < r.Start || j > r.End {
			tmp = append(tmp, j)
		}
	}
	l.songOrder = tmp
}

func (l *orderedSongList) Swap(from, to int) {
	l.songs[from], l.songs[to] = l.songs[to], l.songs[from]
	for i, j := range l.songOrder {
		if j == from {
			l.songOrder[i] = to
		} else if j == to {
			l.songOrder[i] = from
		}
	}
}

func (l *orderedSongList) Move(from Range, pos int) {
	from.Bound(len(l.songs))
	to := Range{Start: pos, End: pos + from.Len()}

	getpos := func(pos int) int {
		if (pos < from.Start && pos < to.Start) || (pos >= from.End && pos >= to.End) {
			return pos
		}
		if to.Contains(pos) {
			return pos + from.Start - to.Start
		}
		if from.Start > to.Start {
			return pos - from.Len()
		}
		return pos + from.Len()
	}

	newsongs := make([]Song, len(l.songs))
	for i := 0; i < len(l.songs); i++ {
		newsongs[i] = l.songs[getpos(i)]
	}
	l.songs = newsongs
}

// Shuffle the song ordering starting at position minPos.
func (l *orderedSongList) Shuffle(minPos int) {
	var tmp []int
	if minPos < 0 || minPos >= len(l.songOrder)-1 {
		// Just do a full shuffle.
		minPos = 0
	}
	if minPos > 0 {
		copy(tmp[:minPos-1], l.songOrder[:minPos-1])
	}
	for oidx, nidx := range rand.Perm(len(l.songOrder) - minPos) {
		tmp[minPos+nidx] = l.songOrder[minPos+oidx]
	}
	l.songOrder = tmp
}

type playlistSong struct {
	Song
	idx int
}

func (s *playlistSong) ID() int {
	return s.idx + 1
}

func (s *playlistSong) Index() int {
	return s.idx
}

func (s *playlistSong) GetSong() Song {
	return s.Song
}

// A Playlist contains an ordered list of songs, and a cursor.
type Playlist struct {
	Version int
	songs   *orderedSongList

	// Offset of the current song in the ordered list.
	curPos int

	doRandom bool
	doRepeat bool

	lock sync.Mutex
}

func NewPlaylist() *Playlist {
	return &Playlist{
		Version: 1,
		songs:   new(orderedSongList),
		curPos:  -1,
	}
}

// Len returns the number of songs currently in the playlist.
func (p *Playlist) Len() int {
	p.lock.Lock()
	defer p.lock.Unlock()
	return p.songs.Len()
}

// Makes sure that no matter what alterations are performed on the
// song list, the current cursor points at the same song if possible.
// This isn't fast, but reasonably effective.
func (p *Playlist) cursorInvariant(f func()) {
	curSong := p.songs.SongAtPos(p.curPos)
	f()
	if curSong != nil {
		if newpos := p.songs.PositionOf(curSong); newpos >= 0 {
			p.curPos = newpos
		} else {
			p.curPos = -1
		}
	}
}

// Clear the playlist.
func (p *Playlist) Clear() {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.songs = new(orderedSongList)
	p.curPos = -1
	p.Version++
}

// SetRandom sets the playlist random mode on or off.
func (p *Playlist) SetRandom(state bool) {
	if state && p.doRandom != state {
		p.lock.Lock()
		defer p.lock.Unlock()
		p.cursorInvariant(func() {
			p.songs.Shuffle(0)
		})
	}
	p.doRandom = state
}

// SetRepeat toggles the playlist repeat mode.
func (p *Playlist) SetRepeat(state bool) {
	p.doRepeat = state
}

// Random returns the current state of the playlist random mode.
func (p *Playlist) Random() bool { return p.doRandom }

// Repeat returns the current state of the playlist repeat mode.
func (p *Playlist) Repeat() bool { return p.doRepeat }

// Next advances the cursor to the next song and returns it.
func (p *Playlist) Next() SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.songs.Empty() {
		return nil
	}

	p.curPos++
	if p.curPos >= p.songs.Len() {
		if p.doRepeat {
			if p.doRandom {
				p.songs.Shuffle(0)
			}
			p.curPos = 0
		} else {
			log.Printf("Next() reached end of playlist")
			p.curPos = -1
			return nil
		}
	}
	return &playlistSong{p.songs.SongAtPos(p.curPos), p.songs.IndexAtPos(p.curPos)}
}

// PeekNext looks at the next iteration of the cursor, but does not
// advance it.
func (p *Playlist) PeekNext() SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.songs.Empty() {
		return nil
	}

	pos := p.curPos + 1
	if pos >= p.songs.Len() {
		if p.doRepeat {
			// Uhh, we are actually lying here.
			pos = 0
		} else {
			return nil
		}
	}
	return &playlistSong{p.songs.SongAtPos(pos), p.songs.IndexAtPos(pos)}
}

// Prev moves the cursor back to the previous song and returns it.
func (p *Playlist) Prev() SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.songs.Empty() {
		return nil
	}

	p.curPos--
	if p.curPos < 0 {
		if p.doRepeat {
			if p.doRandom {
				p.songs.Shuffle(0)
			}
			p.curPos = p.songs.Len() - 1
		} else {
			log.Printf("Prev() reached beginning of playlist")
			// Just in case someone calls Prev multiple times.
			p.curPos = -1
			return nil
		}
	}
	return &playlistSong{p.songs.SongAtPos(p.curPos), p.songs.IndexAtPos(p.curPos)}
}

// SetCursor moves the cursor to point at the specified song, and
// returns it.
func (p *Playlist) SetCursor(idx int) SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()

	song := p.songs.SongAtIndex(idx)
	p.curPos = p.songs.PositionOf(song)
	return &playlistSong{song, idx}
}

// SetCursorID moves the cursor to point at the specified song (using
// its ID), and returns it.
func (p *Playlist) SetCursorID(id int) SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()

	// -1 means stop.
	if id == -1 {
		p.curPos = -1
		return nil
	}

	idx := p.idToIndex(id)
	song := p.songs.SongAtIndex(idx)
	p.curPos = p.songs.PositionOf(song)
	return &playlistSong{song, idx}
}

// Add adds a new song to the playlist, returns the song ID.
func (p *Playlist) Add(s Song, before int) int {
	p.lock.Lock()
	defer p.lock.Unlock()

	var idx int
	p.cursorInvariant(func() {
		idx = p.songs.Add(s, before)
		if p.doRandom {
			p.songs.Shuffle(p.curPos)
		}
	})
	p.Version++
	return p.indexToID(idx)
}

// Convert between song index and song ID. Song IDs in reality are
// just str(index + 1).
func (p *Playlist) indexToID(idx int) int {
	return idx + 1
}

// Convert between song ID and song index.
func (p *Playlist) idToIndex(id int) int {
	return id - 1
}

func (p *Playlist) dumpSongInfoWithID(w io.Writer, song Song, idx int) {
	io.WriteString(w, song.Info())
	fmt.Fprintf(w, "Id: %d\nPos: %d\n", p.indexToID(idx), idx)
}

// Info dumps information about all the songs in the playlist to w.
func (p *Playlist) Info(w io.Writer) {
	p.lock.Lock()
	defer p.lock.Unlock()
	for idx, song := range p.songs.songs {
		p.dumpSongInfoWithID(w, song, idx)
	}
}

// Info dumps information about the songs in the specified range.
func (p *Playlist) InfoRange(w io.Writer, r Range) {
	p.lock.Lock()
	defer p.lock.Unlock()
	if p.songs.Len() == 0 {
		return
	}
	r.Bound(len(p.songs.songs))
	for idx, song := range p.songs.songs[r.Start:r.End] {
		p.dumpSongInfoWithID(w, song, r.Start+idx)
	}
}

// InfoID dumps information about a song to w.
func (p *Playlist) InfoID(w io.Writer, id int) {
	p.lock.Lock()
	defer p.lock.Unlock()
	idx := p.idToIndex(id)
	song := p.songs.SongAtIndex(idx)
	if song != nil {
		p.dumpSongInfoWithID(w, song, idx)
	} else {
		// Return first song, if available.
		log.Printf("warning: playlistid %d matches no songs", id)
		// song := p.songs.SongAtIndex(0)
		// if song != nil {
		// 	p.dumpSongInfoWithID(w, song, 0)
		// }
	}
}

func findMatch(song Song, tag, needle string) bool {
	switch tag {
	case "file":
		return song.URL() == needle
	}
	return false
}

// Find runs a literal query on the current playlist.
func (p *Playlist) Find(w io.Writer, tag, needle string) {
	p.lock.Lock()
	defer p.lock.Unlock()

	for idx, song := range p.songs.songs {
		if findMatch(song, tag, needle) {
			p.dumpSongInfoWithID(w, song, idx)
		}
	}
}

// ChangesPosID prints to w the difference between the specified
// playlist version and the current one, encoded in the differential
// format specified by the MPD protocol. At the moment, since playlist
// history is not supported, this function pretends that the playlist
// changed completely.
func (p *Playlist) ChangesPosID(w io.Writer, oldver int) {
	for idx := range p.songs.songs {
		fmt.Fprintf(w, "cpos: %d\nId: %d\n", idx, idx+1)
	}
}

// Delete a range of songs from the playlist.
func (p *Playlist) Delete(r Range) {
	p.lock.Lock()
	defer p.lock.Unlock()

	p.cursorInvariant(func() {
		p.songs.Delete(r)
	})
	p.Version++
}

// DeleteID removes a specific song from the playlist.
func (p *Playlist) DeleteID(id int) {
	idx := p.idToIndex(id)
	p.Delete(singleIndexRange(idx))
}

// Swap the position of two songs.
func (p *Playlist) Swap(from, to int) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.cursorInvariant(func() {
		p.songs.Swap(from, to)
	})
	p.Version++
}

// SwapID changes the position of two songs (the first song is
// specified with its ID).
func (p *Playlist) SwapID(id, to int) {
	from := p.idToIndex(id)
	if from > 0 {
		p.Swap(from, to)
	}
}

// Move the song at position from to position to.
func (p *Playlist) Move(from Range, to int) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.songs.Move(from, to)
	p.Version++
}

// Move the specified song to position to.
func (p *Playlist) MoveID(id, to int) {
	from := p.idToIndex(id)
	if from > 0 {
		p.Move(singleIndexRange(from), to)
	}
}
