package mpd

import (
	"encoding/binary"
	"io"
	"log"
	"os"
	"os/exec"
)

var avconv = "avconv"

func init() {
	// Let the users override the location of the 'avconv' binary
	// using an environment variable.
	avconvEnv := os.Getenv("AVCONV")
	if avconvEnv != "" {
		avconv = avconvEnv
	}

	// If we can't find the avconv/ffmpeg executable, bail out and
	// warn the user.
	if _, err := exec.LookPath(avconv); err != nil {
		log.Fatal("Can't find \"avconv\" in your PATH, please install it (\"sudo apt-get install libav-tools\" on Debian-based systems). You can also use \"ffmpeg\", and use the AVCONV environment variable to point this application at it.")
	}
}

type decoder struct {
	src    io.ReadCloser
	cmd    *exec.Cmd
	stdin  io.WriteCloser
	stdout io.ReadCloser
}

func (d *decoder) Read(buf []byte) (int, error) {
	return d.stdout.Read(buf)
}

func (d *decoder) Close() error {
	d.src.Close()
	d.cmd.Process.Kill()
	err := d.cmd.Wait()
	log.Printf("ffmpeg pid %d stopped (%v)", d.cmd.Process.Pid, err)
	return err
}

// Decode an io.Reader using avconv/ffmpeg.
func Decode(src io.ReadCloser, format string) (io.ReadCloser, error) {
	cmd := exec.Command(avconv, "-i", "-", "-f", format, "-")
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		stdin.Close()
		return nil, err
	}

	if err := cmd.Start(); err != nil {
		stdin.Close()
		stdout.Close()
		return nil, err
	}

	log.Printf("ffmpeg pid %d started", cmd.Process.Pid)

	go func() {
		// Copy 'src' to the standard input.
		io.Copy(stdin, src)
		// Close stdin to force avconv to exit.
		stdin.Close()
	}()

	// Return the wrapped stdout.
	return &decoder{src, cmd, stdin, stdout}, nil
}

// bufferedReader ensures no short reads.
type bufferedReader struct {
	io.Reader
}

func (r *bufferedReader) Read(buf []byte) (int, error) {
	p := 0
	for p < len(buf) {
		n, err := r.Reader.Read(buf[p:len(buf)])
		if err != nil {
			return p, err
		}
		if n == 0 {
			break
		}
		p += n
	}
	return p, nil
}

// Buffer pool to limit GC.
type bufferPool struct {
	bufsize int
	ch      chan []int16
}

var audioBufferPool = &bufferPool{
	bufsize: audioBufFrames * 2,
	ch:      make(chan []int16, audioBufQueueSize),
}

func (p *bufferPool) New() []int16 {
	var b []int16
	select {
	case b = <-p.ch:
	default:
		b = make([]int16, p.bufsize)
	}
	return b
}

func (p *bufferPool) Free(b []int16) {
	select {
	case p.ch <- b:
	default:
	}
}

// decodeSamples reads bytes and writes samples to a channel.
func decodeSamples(src io.Reader) <-chan []int16 {
	ch := make(chan []int16, audioBufQueueSize)

	go func() {
		// We use a local buffer and a sample decoding loop instead of
		// a plain binary.Read because we want to handle gracefully
		// the last (partial) buffer...
		defer close(ch)
		encoding := binary.LittleEndian
		bbuf := make([]byte, audioBufFrames*2*2)
		bufn := 0
		for {
			n, err := src.Read(bbuf)
			if err != nil {
				return
			}
			if n == 0 {
				return
			}

			if bufn == 0 {
				log.Println("received decoded data")
			}
			bufn++

			// Decode the binary input buffer to samples.
			abuf := audioBufferPool.New()
			for i := 0; i < n/2; i++ {
				abuf[i] = int16(encoding.Uint16(bbuf[i*2 : i*2+2]))
			}
			ch <- abuf
		}
	}()

	return ch
}
