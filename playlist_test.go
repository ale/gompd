package mpd

import (
	"io"
	"reflect"
	"strconv"
	"testing"
)

func TestRange_ParseRange(t *testing.T) {
	p, err := ParseRange("0:3")
	if err != nil {
		t.Fatalf("[0:3]: %v", err)
	}
	if p.Start != 0 || p.End != 4 {
		t.Fatalf("[0:3]: expected {0, 4} got %v", p)
	}

	p, err = ParseRange("2")
	if err != nil {
		t.Fatalf("[2]: %v", err)
	}
	if p.Start != 2 || p.End != 3 {
		t.Fatalf("[2]: expected {2, 3} got %v", p)
	}
}

type testSong struct {
	id string
}

func (s testSong) URL() string         { return "url" }
func (s testSong) Info() string        { return "info" }
func (s testSong) Channels() int       { return 2 }
func (s testSong) SampleRate() float64 { return 44100 }
func (s testSong) Duration() int       { return 100 }

type testReadCloser struct{}

func (r testReadCloser) Read(buf []byte) (int, error) {
	return 0, io.EOF
}

func (r testReadCloser) Close() error { return nil }

func (s testSong) Open() (io.ReadCloser, error) {
	return &testReadCloser{}, nil
}

func checkList(t *testing.T, sl *orderedSongList, expected []string) {
	if sl.Len() != len(expected) {
		t.Fatalf("Len() is %d, expected %d", sl.Len(), len(expected))
	}

	var got []string
	for i := 0; i < sl.Len(); i++ {
		s := sl.SongAtPos(i)
		got = append(got, s.(*testSong).id)
	}

	if !reflect.DeepEqual(got, expected) {
		t.Fatalf("Error in song list: got=%v, expected=%v", got, expected)
	}
}

func TestOrderedSongList_IndexPositionCoherence(t *testing.T) {
	var sl orderedSongList
	song1 := &testSong{"1"}
	song2 := &testSong{"2"}
	sl.Add(song1, 0)
	sl.Add(song2, 0)

	if sl.Len() != 2 {
		t.Fatalf("Len() is %d, expected 2", sl.Len())
	}

	if s := sl.SongAtPos(0); s != song1 {
		t.Error("SongAtPos(0) is not song1")
	}
	if s := sl.SongAtPos(1); s != song2 {
		t.Error("SongAtPos(1) is not song2")
	}
	if sl.SongAtPos(2) != nil {
		t.Error("SongAtPos(2) is not nil")
	}

	if s := sl.SongAtIndex(0); s != song1 {
		t.Error("SongAtIndex(0) is not song1")
	}
	if s := sl.SongAtIndex(1); s != song2 {
		t.Error("SongAtIndex(1) is not song2")
	}
	if sl.SongAtIndex(2) != nil {
		t.Error("SongAtIndex(2) is not nil")
	}

	if pos := sl.PositionOf(song1); pos != 0 {
		t.Errorf("PositionOf(song1) is %d, expected 0", pos)
	}
	if pos := sl.PositionOf(song2); pos != 1 {
		t.Errorf("PositionOf(song2) is %d, expected 1", pos)
	}
	if pos := sl.PositionOf(&testSong{"3"}); pos != -1 {
		t.Errorf("PositionOf(new song) is %d, expected -1", pos)
	}

	// Without a shuffle, order is known.
	for pos := 0; pos < 2; pos++ {
		if idx := sl.IndexAtPos(pos); idx != pos {
			t.Errorf("IndexAtPos(%d) is %d, expected %d", pos, idx, pos)
		}
	}
}

func createOrderedSongList(size int) *orderedSongList {
	var sl orderedSongList
	for i := 0; i < size; i++ {
		sl.Add(&testSong{strconv.Itoa(i + 1)}, 0)
	}
	return &sl
}

func TestOrderedSongList_Delete(t *testing.T) {
	sl := createOrderedSongList(3)
	sl.Delete(Range{Start: 1, End: 3})
	checkList(t, sl, []string{"1"})
}

func TestOrderedSongList_Move(t *testing.T) {
	testdata := []struct {
		from Range
		to   int
		exp  []string
	}{
		{Range{3, 6}, 1, []string{"1", "4", "5", "6", "2", "3"}},
		{Range{0, 2}, 3, []string{"3", "4", "5", "1", "2", "6"}},
		{Range{0, 1}, 5, []string{"2", "3", "4", "5", "6", "1"}},
		{Range{5, 6}, 0, []string{"6", "1", "2", "3", "4", "5"}},
	}
	for _, tt := range testdata {
		t.Logf("Move(from=%v, to=%v)", tt.from, tt.to)
		sl := createOrderedSongList(6)
		sl.Move(tt.from, tt.to)
		checkList(t, sl, tt.exp)
	}
}

func testPlaylist(size int) *Playlist {
	pl := NewPlaylist()
	for i := 0; i < size; i++ {
		pl.Add(&testSong{strconv.Itoa(i + 1)}, 0)
	}
	return pl
}

func TestPlaylist_Delete(t *testing.T) {
	pl := testPlaylist(3)
	pl.Delete(Range{Start: 1, End: 3})
	checkList(t, pl.songs, []string{"1"})
}

func TestPlaylist_DeleteID(t *testing.T) {
	pl := testPlaylist(3)
	pl.DeleteID(3)
	checkList(t, pl.songs, []string{"1", "2"})
}

func TestPlaylist_Next(t *testing.T) {
	pl := testPlaylist(3)
	for i := 0; i < 3; i++ {
		song := pl.Next()
		if song.Index() != i {
			t.Errorf("Next(@%d) returned song %v", i, song)
		}
	}
	if song := pl.Next(); song != nil {
		t.Errorf("Next(end) returned non-nil: %v", song)
	}
}

func TestPlaylist_Prev(t *testing.T) {
	pl := testPlaylist(3)
	if song := pl.Prev(); song != nil {
		t.Fatalf("Prev(beg) returned non-nil: %v", song)
	}

	pl.Next()
	if song := pl.Prev(); song != nil {
		t.Fatalf("Prev(0) returned non-nil: %v", song)
	}

	pl.Next()
	pl.Next()
	if song := pl.Prev(); song == nil {
		t.Error("Prev(1) returned nil")
	}
}
