package mpd

import (
	"errors"
	"fmt"
	"io"
	"log"
	"sync"

	"github.com/HardWareGuy/portaudio-go"
)

const (
	// Audio buffer size (in frames).
	audioBufFrames = 4096

	// Size of the audio buffer queue.
	audioBufQueueSize = 16
)

type audioParams struct {
	channels   int
	samplerate float64
}

// Keep an audio device always open, reconfigure it only when necessary.
type audioDevice struct {
	// Internal buffer. This limits the sample size to 16 bits, as
	// portaudio has trouble with 24 bits per sample (result of a test
	// with a high-end USB audio device: no output).
	c       chan []int16
	buf     []int16
	params  audioParams
	device  *portaudio.DeviceInfo
	stream  *portaudio.Stream
	started bool
	lock    sync.Mutex
}

func newAudioDevice() (*audioDevice, error) {
	defaultOutputDev, err := portaudio.DefaultOutputDevice()
	if err != nil {
		return nil, err
	}
	a := &audioDevice{
		c:   make(chan []int16, 1),
		buf: make([]int16, audioBufFrames*2),
		params: audioParams{
			channels:   2,
			samplerate: 44100,
		},
		device: defaultOutputDev,
	}
	if err := a.open(); err != nil {
		return nil, err
	}
	go a.audioLoop()
	return a, nil
}

func (a *audioDevice) audioLoop() {
	for buf := range a.c {
		copy(a.buf, buf)
		audioBufferPool.Free(buf)

		// Write() will block, don't hold the lock.
		if stream := a.stream; stream != nil {
			if err := stream.Write(); err == portaudio.OutputUnderflowed {
				log.Println("output underflowed")
			} else if err != nil {
				log.Printf("audio error: %v", err)
			}
		}
	}
}

func (a *audioDevice) SetParameters(p audioParams) error {
	if p == a.params {
		return nil
	}
	// We may need to reallocate the buffer.
	if p.channels != a.params.channels {
		a.buf = make([]int16, audioBufFrames*p.channels)
	}
	a.params = p
	return a.open()
}

func (a *audioDevice) SetDevice(device *portaudio.DeviceInfo) error {
	if device == a.device {
		return nil
	}
	a.device = device
	return a.open()
}

func (a *audioDevice) Start() {
	a.lock.Lock()
	defer a.lock.Unlock()
	if a.stream != nil {
		a.stream.Start()
		a.started = true
	}
}

func (a *audioDevice) Stop() {
	a.lock.Lock()
	defer a.lock.Unlock()
	if a.stream != nil {
		a.stream.Stop()
		a.started = false
	}
}

func (a *audioDevice) open() error {
	a.lock.Lock()
	defer a.lock.Unlock()
	if a.stream != nil {
		a.stream.Stop()
		a.stream.Close()
	}

	// Attempt to open the device. We rely on the return value
	// being nil if err is set.
	var err error
	log.Printf("open audio device \"%s\" %+v", a.device.Name, a.params)
	a.stream, err = portaudio.OpenStream(a.streamParameters(), &a.buf)

	// For a seamless switch while a songPlayer goroutine is
	// active, we need to restart the stream.
	if err == nil && a.started {
		a.stream.Start()
	}

	return err
}

func (a *audioDevice) streamParameters() portaudio.StreamParameters {
	return portaudio.StreamParameters{
		Output: portaudio.StreamDeviceParameters{
			Device:   a.device,
			Latency:  a.device.DefaultHighOutputLatency,
			Channels: a.params.channels,
		},
		SampleRate:      a.params.samplerate,
		FramesPerBuffer: audioBufFrames,
	}
}

const (
	opPlay = iota
	opStop
	opPause
)

type playerCommand struct {
	op   int
	song SongFromPlaylist
}

type songSource struct {
	c       <-chan []int16
	decoder io.ReadCloser
	songfd  io.ReadCloser
}

func (s *songSource) Close() error {
	//s.songfd.Close()
	s.decoder.Close()
	// Flush the buffered channel.
	for _ = range s.c {
	}
	return nil
}

// Create a new song source. This is a chain of io.Readers ultimately
// feeding constant-sized buffers into a channel. Since it is expected
// that the source is remote, we read from it as fast as we can and
// cache the results to disk.
func newSongSource(song SongFromPlaylist) (*songSource, error) {
	songfd, err := song.Open()
	if err != nil {
		return nil, err
	}

	// cr, err := newDiskBackedReader(songfd)
	// if err != nil {
	// 	songfd.Close()
	// 	return nil, err
	// }

	decoder, err := Decode(songfd, "s16le")
	if err != nil {
		songfd.Close()
		return nil, err
	}

	return &songSource{
		c:       decodeSamples(&bufferedReader{decoder}),
		decoder: decoder,
		songfd:  songfd,
	}, nil
}

type SongPlayNotifier interface {
	Notify(Song)
}

type SongProvider interface {
	Next() SongFromPlaylist
}

// Main audio player logic.
type audioPlayer struct {
	state         PlayerState
	currentSong   SongFromPlaylist
	currentSource *songSource
	audioDev      *audioDevice
	ctrl          chan playerCommand
	songNotifier  SongPlayNotifier
	songProvider  SongProvider
	lock          sync.Mutex
}

func newAudioPlayer(dev *audioDevice, provider SongProvider, notifier SongPlayNotifier) *audioPlayer {
	a := &audioPlayer{
		audioDev:     dev,
		state:        StateStop,
		songProvider: provider,
		songNotifier: notifier,
		ctrl:         make(chan playerCommand),
	}
	go a.loop()
	return a
}

func (p *audioPlayer) loop() {
	for {
		switch p.state {
		case StatePlay:
			select {
			case buf, ok := <-p.currentSource.c:
				if !ok {
					// The current song has ended. Notify
					// the caller in a separate goroutine,
					// and play the next song (if any).
					go p.songNotifier.Notify(p.currentSong.GetSong())
					song := p.songProvider.Next()
					if song == nil {
						p.handleCmd(playerCommand{op: opStop})
					} else {
						p.handleCmd(playerCommand{op: opPlay, song: song})
					}
				} else {
					p.audioDev.c <- buf
				}
			case cmd := <-p.ctrl:
				p.handleCmd(cmd)
			}
		default:
			cmd := <-p.ctrl
			p.handleCmd(cmd)
		}
	}
}

func (p *audioPlayer) handleCmd(cmd playerCommand) {
	p.lock.Lock()
	defer p.lock.Unlock()
	switch {
	case cmd.op == opPlay:
		if p.state != StateStop {
			p.stopSource()
		}
		if err := p.startSource(cmd.song); err != nil {
			log.Printf("error playing song %s: %v", cmd.song.URL(), err)
			p.state = StateStop
		} else {
			p.state = StatePlay
		}
	case cmd.op == opStop && p.state != StateStop:
		p.stopSource()
		p.audioDev.Stop()
		p.state = StateStop
	case cmd.op == opPause:
		switch p.state {
		case StatePlay:
			p.audioDev.Stop()
			p.state = StatePause
		case StatePause:
			p.audioDev.Start()
			p.state = StatePlay
		}
	}
}

func (p *audioPlayer) startSource(song SongFromPlaylist) error {
	params := audioParams{
		channels:   song.Channels(),
		samplerate: song.SampleRate(),
	}
	if err := p.audioDev.SetParameters(params); err != nil {
		return fmt.Errorf("error setting parameters for audio device (%v): %v", params, err)
	}

	src, err := newSongSource(song)
	if err != nil {
		return err
	}
	p.currentSong = song
	p.currentSource = src

	p.audioDev.Start()
	return nil
}

func (p *audioPlayer) stopSource() {
	// Cleanup the current source in a separate goroutine.
	if p.currentSource != nil {
		go p.currentSource.Close()
	}
	p.currentSong = nil
	p.currentSource = nil
}

func (p *audioPlayer) Play(song SongFromPlaylist) {
	if song == nil {
		song = p.songProvider.Next()
	}
	if song == nil {
		return
	}

	p.ctrl <- playerCommand{op: opPlay, song: song}
}

func (p *audioPlayer) Pause() {
	p.ctrl <- playerCommand{op: opPause}
}

func (p *audioPlayer) Stop() {
	p.ctrl <- playerCommand{op: opStop}
}

func (p *audioPlayer) State() PlayerState {
	p.lock.Lock()
	defer p.lock.Unlock()
	return p.state
}

func (p *audioPlayer) CurSong() SongFromPlaylist {
	p.lock.Lock()
	defer p.lock.Unlock()
	return p.currentSong
}

// An audioPlayer that knows about PortAudio hw devices.
type portAudioPlayer struct {
	*audioPlayer

	outputDevs   []*portaudio.DeviceInfo
	curOutputDev *portaudio.DeviceInfo

	volume int
}

func NewPortAudioPlayer(provider SongProvider, notifier SongPlayNotifier) *portAudioPlayer {
	portaudio.Initialize()
	audiodev, err := newAudioDevice()
	if err != nil {
		log.Fatal(err)
	}
	p := &portAudioPlayer{
		audioPlayer:  newAudioPlayer(audiodev, provider, notifier),
		curOutputDev: audiodev.device,
		volume:       100,
	}
	p.updateDevices()
	return p
}

// Close the audio player and release all resources.
func (p *portAudioPlayer) Close() {
	portaudio.Terminate()
}

// Update the list of available output devices (according to PortAudio).
func (p *portAudioPlayer) updateDevices() {
	// Ignore errors here for the moment.
	var out []*portaudio.DeviceInfo
	devs, _ := portaudio.Devices()
	for _, d := range devs {
		if d.MaxOutputChannels > 0 {
			out = append(out, d)
		}
	}
	p.outputDevs = out
}

func (p *portAudioPlayer) Outputs() []Output {
	var out []Output
	for idx, d := range p.outputDevs {
		out = append(out, Output{
			ID:      idx,
			Name:    d.Name,
			Enabled: d == p.curOutputDev,
		})
	}
	return out
}

func (p *portAudioPlayer) SetOutput(idx int) error {
	if idx < 0 || idx >= len(p.outputDevs) {
		return errors.New("no such device")
	}
	if err := p.audioDev.SetDevice(p.outputDevs[idx]); err != nil {
		p.audioDev.SetDevice(p.curOutputDev)
		return err
	}
	p.curOutputDev = p.outputDevs[idx]
	return nil
}

func (p *portAudioPlayer) Volume() int {
	return p.volume
}

func (p *portAudioPlayer) SetVolume(vol int) {
	// This doesn't do anything right now.
	p.volume = vol
}

func (p *portAudioPlayer) SongTime() int {
	// TODO: implement this again.
	return 0
}
