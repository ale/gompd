package mpd

import (
	"fmt"
	"io"
	"log"
	"net/url"
	"strconv"
	"time"
)

const (
	StateStop = iota
	StatePlay
	StatePause
)

// FIXME: there are currently no stats.
type stats struct {
	artists int
	albums  int
	songs   int
}

// A song, with associated metadata and audio resource.
type Song interface {
	// URL returns a unique identifier for this song.
	URL() string

	// Info returns a string with the song metadata formatted
	// according to the MPD protocol.
	Info() string

	// Open the audio file and return it (in the original format,
	// which will be decoded by the player). The caller must call
	// Close() on the result.
	Open() (io.ReadCloser, error)

	Channels() int
	SampleRate() float64
	Duration() int
}

// A song, with associated playlist information.
type SongFromPlaylist interface {
	Song

	// ID returns the song id in the playlist.
	ID() int

	// Index of this song in the playlist.
	Index() int

	// Song returns the wrapped Song.
	GetSong() Song
}

// Database is used to search for songs. Queries are encoded according
// to the MPD protocol, consisting of paired tag/query fields, and
// must be decoded by the implementation.
type Database interface {
	// Find an exact match for the query.
	Find(args []string) ([]Song, error)

	// Search the database with the given query.
	Search(args []string) ([]Song, error)
}

// A function that returns the next song to be played.
type SongCallbackFunc func() SongFromPlaylist

// Callback called when a song has been successfully played in its
// entirety.
type SongPlayedCallbackFunc func(Song)

// Audio device.
type Output struct {
	Name    string
	ID      int
	Enabled bool
}

type PlayerState int

func (s PlayerState) String() string {
	switch s {
	case StateStop:
		return "stop"
	case StatePlay:
		return "play"
	case StatePause:
		return "pause"
	}
	return ""
}

// A URLHandler is associated with a specific URL scheme, and it is
// used to retrieve songs (possibly from a database).
type URLHandler interface {
	// GetSong returns the Song identified by the given url.
	GetSong(songURL *url.URL) (Song, error)
}

// MPD server. Implements most of the commands of a real server, but
// it is oriented towards searching rather than browsing (that is to
// say, browsing is currently unimplemented).
type MPD struct {
	*Server

	// Tag types. Currently not used.
	TagTypes []string

	lastError string

	player   *portAudioPlayer
	playlist *Playlist
	database Database

	stats     stats
	startedAt time.Time

	urlHandlers map[string]URLHandler
}

func (m *MPD) getSong(songURL string) (Song, error) {
	url, err := url.Parse(songURL)
	if err != nil {
		return nil, err
	}
	h, ok := m.urlHandlers[url.Scheme]
	if !ok {
		return nil, ErrUnsupportedScheme
	}
	return h.GetSong(url)
}

// This and the following functions are just syntactical glue to make
// it easy to express the large number of commands in the MPD
// protocol, most of which do very similar things.
//
// RN in function names stands for 'returns nil' (in which case the
// io.Writer argument is also omitted).
func withArgs(f func(io.Writer, []string) error, minArgs, maxArgs int) Handler {
	return CommandFunc(func(w io.Writer, args []string) error {
		if len(args) < minArgs || len(args) > maxArgs {
			return ErrBadArgs
		}
		return f(w, args)
	})
}

func withOneArg(f func(io.Writer, string) error) Handler {
	return withArgs(func(w io.Writer, args []string) error {
		return f(w, args[0])
	}, 1, 1)
}

func withTwoArgsRN(f func(io.Writer, string, string)) Handler {
	return withArgs(func(w io.Writer, args []string) error {
		f(w, args[0], args[1])
		return nil
	}, 2, 2)
}

func withInt(f func(io.Writer, int) error) Handler {
	return withOneArg(func(w io.Writer, arg string) error {
		value, err := strconv.Atoi(arg)
		if err != nil {
			return err
		}
		return f(w, value)
	})
}

func withIntRN(f func(int)) Handler {
	return withOneArg(func(w io.Writer, arg string) error {
		value, err := strconv.Atoi(arg)
		if err != nil {
			return err
		}
		f(value)
		return nil
	})
}

func withTwoIntsRN(f func(int, int)) Handler {
	return withArgs(func(w io.Writer, args []string) error {
		value1, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}
		value2, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}
		f(value1, value2)
		return nil
	}, 2, 2)
}

func withNoOutput(f func()) Handler {
	return withArgs(func(w io.Writer, args []string) error {
		f()
		return nil
	}, 0, 0)
}

func NewMPD(db Database, notifier SongPlayNotifier) *MPD {
	// Create and connect a player and a playlist.
	playlist := NewPlaylist()
	player := NewPortAudioPlayer(playlist, notifier)
	m := &MPD{
		Server:      NewServer(),
		player:      player,
		playlist:    playlist,
		database:    db,
		TagTypes:    []string{"Artist", "Album", "Title", "Genre"},
		startedAt:   time.Now(),
		urlHandlers: make(map[string]URLHandler),
	}

	// Exactly one output device can be enabled at any time, so
	// most of these commands probably won't work as expected if
	// used in non-obvious ways.
	m.HandleFunc("outputs", m.cmdOutputs)
	m.Handle("enableoutput", withIntRN(func(id int) {
		m.player.SetOutput(id)
	}))
	m.HandleFunc("disableoutput", nopCmd)
	m.HandleFunc("toggleoutput", nopCmd)

	// Playback options.
	m.Handle("setvol", withInt(m.cmdSetVol))
	m.Handle("random", m.withNotify("options", withIntRN(func(state int) {
		m.playlist.SetRandom(state == 1)
	})))
	m.Handle("repeat", m.withNotify("options", withIntRN(func(state int) {
		m.playlist.SetRepeat(state == 1)
	})))

	// Status.
	m.HandleFunc("status", m.cmdStatus)
	m.HandleFunc("stats", m.cmdStats)
	m.HandleFunc("currentsong", m.cmdCurrentSong)
	m.Handle("clearerror", withNoOutput(func() {
		m.lastError = ""
	}))

	// Reflection.
	m.HandleFunc("urlhandlers", m.cmdURLHandlers)
	m.HandleFunc("tagtypes", m.cmdTagTypes)

	// Playlist commands.
	m.Handle("add", m.withNotify("playlist", withOneArg(m.cmdAdd)))
	m.Handle("addid", m.withNotify("playlist", withOneArg(m.cmdAddID)))
	m.Handle("clear", m.withNotify("playlist", withNoOutput(m.playlist.Clear)))
	m.Handle("delete", m.withNotify("playlist", withOneArg(func(w io.Writer, arg string) error {
		r, err := ParseRange(arg)
		if err != nil {
			return err
		}
		m.playlist.Delete(r)
		return nil
	})))
	m.Handle("deleteid", m.withNotify("playlist", withIntRN(m.playlist.DeleteID)))
	m.Handle("move", m.withNotify("playlist", withTwoArgsRN(func(w io.Writer, from string, to string) {
		fromRange, err := ParseRange(from)
		if err != nil {
			return
		}
		toInt, err := strconv.Atoi(to)
		if err != nil {
			return
		}
		m.playlist.Move(fromRange, toInt)
	})))
	m.Handle("moveid", m.withNotify("playlist", withTwoIntsRN(m.playlist.MoveID)))
	m.Handle("swap", m.withNotify("playlist", withTwoIntsRN(m.playlist.Swap)))
	m.Handle("swapid", m.withNotify("playlist", withTwoIntsRN(m.playlist.SwapID)))
	m.HandleFunc("playlistinfo", m.cmdPlaylistInfo)
	m.HandleFunc("playlistid", m.cmdPlaylistID)
	m.Handle("plchanges", withInt(m.cmdPlaylistChanges))
	m.Handle("plchangesposid", withInt(m.cmdPlaylistChangesPosID))
	m.Handle("playlistfind", withTwoArgsRN(m.playlist.Find))
	// m.HandleFunc("playlist", m.cmdPlaylistURLs)

	// Play control.
	m.Handle("next", m.withNotify("player", withNoOutput(func() {
		m.player.Play(m.playlist.Next())
	})))
	m.Handle("previous", m.withNotify("player", withNoOutput(func() {
		m.player.Play(m.playlist.Prev())
	})))
	m.Handle("stop", m.withNotify("player", withNoOutput(m.player.Stop)))
	m.Handle("pause", m.withNotify("player", CommandFunc(func(w io.Writer, args []string) error {
		// Ignore optional argument.
		m.player.Pause()
		return nil
	})))
	m.Handle("play", m.withNotify("player", CommandFunc(m.cmdPlay)))
	m.Handle("playid", m.withNotify("player", CommandFunc(m.cmdPlayID)))

	// Database management.
	m.HandleFunc("search", m.cmdSearch)
	m.HandleFunc("find", m.cmdFind)
	m.HandleFunc("count", m.cmdCount)
	m.HandleFunc("list", nopCmd)
	m.HandleFunc("lsinfo", nopCmd)
	m.HandleFunc("listplaylists", nopCmd)

	return m
}

// Send a notification on the given channel only if the handler succeeds.
func (m *MPD) withNotify(tag string, h Handler) Handler {
	return CommandFunc(func(w io.Writer, args []string) error {
		err := h.ServeMPD(w, args)
		if err == nil {
			m.Trigger(tag)
		}
		return err
	})
}

// HandleURL installs a new URLHandler.
func (m *MPD) HandleURL(schema string, h URLHandler) {
	m.urlHandlers[schema] = h
}

func (m *MPD) cmdURLHandlers(w io.Writer, args []string) error {
	for schema := range m.urlHandlers {
		fmt.Fprintf(w, "handler: %s://\n", schema)
	}
	return nil
}

func (m *MPD) cmdTagTypes(w io.Writer, args []string) error {
	for _, t := range m.TagTypes {
		fmt.Fprintf(w, "tagtype: %s\n", t)
	}
	return nil
}

func (m *MPD) cmdOutputs(w io.Writer, args []string) error {
	for _, o := range m.player.Outputs() {
		fmt.Fprintf(w, "outputid: %d\noutputname: %s\noutputenabled: %d\n", o.ID, o.Name, bool2int(o.Enabled))
	}
	return nil
}

func (m *MPD) cmdEnableOutput(w io.Writer, args []string) error {
	id, _ := strconv.Atoi(args[0])
	m.player.SetOutput(id)
	return nil
}

func (m *MPD) cmdCurrentSong(w io.Writer, args []string) error {
	if m.player.State() == StatePlay {
		if s := m.player.CurSong(); s != nil {
			io.WriteString(w, s.Info())
			fmt.Fprintf(w, "Id: %d\nPos: %d\n", s.ID(), s.Index())
		}
	}
	return nil
}

func (m *MPD) cmdSetVol(w io.Writer, vol int) error {
	if vol < 0 || vol > 100 {
		return ErrBadArgs
	}
	m.player.SetVolume(vol)
	return nil
}

func (m *MPD) cmdStats(w io.Writer, args []string) error {
	uptime := int(time.Now().Sub(m.startedAt).Seconds())
	fmt.Fprintf(w, "artists: %d\nalbums: %d\nsongs: %d\nuptime: %d\nplaytime: 0\n", m.stats.artists, m.stats.albums, m.stats.songs, uptime)
	fmt.Fprintf(w, "db_playtime: 0\ndb_update: %d\n", time.Now().Unix())
	return nil
}

func (m *MPD) cmdStatus(w io.Writer, args []string) error {
	fmt.Fprintf(w, "volume: %d\n", m.player.Volume())
	fmt.Fprintf(w, "repeat: %d\n", bool2int(m.playlist.Repeat()))
	fmt.Fprintf(w, "random: %d\n", bool2int(m.playlist.Random()))
	fmt.Fprintf(w, "state: %s\n", m.player.State())
	fmt.Fprintf(w, "playlist: %d\nplaylistlength: %d\n", m.playlist.Version, m.playlist.Len())
	if m.lastError != "" {
		fmt.Fprintf(w, "error: %s\n", m.lastError)
	}
	if m.player.State() != StateStop {
		if s := m.player.CurSong(); s != nil {
			fmt.Fprintf(w, "song: %d\nsongid: %d\n", s.Index(), s.ID())
			fmt.Fprintf(w, "audio: %d:16:%d\n", int(s.SampleRate()), s.Channels())
			fmt.Fprintf(w, "time: %d:%d\n", m.player.SongTime(), s.Duration())
		}
	}
	if s := m.playlist.PeekNext(); s != nil {
		fmt.Fprintf(w, "nextsong: %d\nnextsongid: %d\n", s.Index(), s.ID())
	}
	return nil
}

func (m *MPD) cmdPlaylistID(w io.Writer, args []string) error {
	if len(args) > 0 {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}
		m.playlist.InfoID(w, id)
	} else {
		m.playlist.Info(w)
	}
	return nil
}

func (m *MPD) cmdPlaylistInfo(w io.Writer, args []string) error {
	if len(args) > 0 {
		r, err := ParseRange(args[0])
		if err != nil {
			return err
		}
		m.playlist.InfoRange(w, r)
	} else {
		m.playlist.Info(w)
	}
	return nil
}

func (m *MPD) cmdPlaylistChanges(w io.Writer, oldver int) error {
	// Pretend all songs have changed.
	m.playlist.Info(w)
	return nil
}

func (m *MPD) cmdPlaylistChangesPosID(w io.Writer, oldver int) error {
	// Pretend all songs have changed.
	m.playlist.ChangesPosID(w, oldver)
	return nil
}

func (m *MPD) cmdAdd(w io.Writer, songURL string) error {
	song, err := m.getSong(songURL)
	if err != nil {
		return err
	}
	m.playlist.Add(song, -1)
	return nil
}

func (m *MPD) cmdAddID(w io.Writer, songURL string) error {
	song, err := m.getSong(songURL)
	if err != nil {
		return err
	}
	id := m.playlist.Add(song, -1)
	fmt.Fprintf(w, "Id: %d\n", id)
	return nil
}

func (m *MPD) cmdPlay(w io.Writer, args []string) error {
	if len(args) > 0 {
		pos, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}
		m.player.Play(m.playlist.SetCursor(pos))
	} else {
		m.player.Play(nil)
	}
	return nil
}

func (m *MPD) cmdPlayID(w io.Writer, args []string) error {
	if len(args) > 0 {
		id, _ := strconv.Atoi(args[0])
		m.player.Play(m.playlist.SetCursorID(id))
	} else {
		m.player.Play(nil)
	}
	return nil
}

func (m *MPD) cmdSearch(w io.Writer, args []string) error {
	if len(args) < 2 {
		return ErrBadArgs
	}
	songs, err := m.database.Search(args)
	if err != nil {
		return err
	}

	log.Printf("search(%v) -> %+v", args, songs)
	for _, s := range songs {
		io.WriteString(w, s.Info())
	}
	return nil
}

func (m *MPD) cmdFind(w io.Writer, args []string) error {
	if len(args) < 2 {
		return ErrBadArgs
	}
	songs, err := m.database.Find(args)
	if err != nil {
		return err
	}

	log.Printf("find(%v) -> %+v", args, songs)
	for _, s := range songs {
		io.WriteString(w, s.Info())
	}
	return nil
}

func (m *MPD) cmdCount(w io.Writer, args []string) error {
	// Currently just a stub.
	io.WriteString(w, "songs: 0\nplaytime: 0\n")
	return nil
}

func bool2int(b bool) int {
	if b {
		return 1
	}
	return 0
}
